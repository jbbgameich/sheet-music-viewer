from PySide2.QtCore import QObject, Signal, Slot, QUrl, QSaveFile, QStandardPaths, QIODevice, QSize, Qt
from PySide2.QtGui import QImage, QPainter, QPageSize, QPagedPaintDevice
from PySide2.QtPrintSupport import QPrinter
from PySide2.QtNetwork import QNetworkAccessManager, QNetworkRequest, QNetworkReply

class Downloader(QObject):
    fileName: str
    sheetImgs: list = []
    numSheetImgs: int

    writeFinished = Signal()
    sheetDownloadFinished = Signal()

    @Slot(QUrl, str)
    def download(self, url: QUrl, fileName: str):
        self.fileName = fileName

        manager = QNetworkAccessManager(self)
        manager.finished[QNetworkReply].connect(self.replyFinished)

        manager.get(QNetworkRequest(url))

    @Slot("QVariantList", str)
    def downloadSheet(self, urls: list, fileName: str):
        self.fileName = "{}.pdf".format(fileName)
        self.numSheetImgs = len(urls)

        self.sheetImgs.clear()

        manager = QNetworkAccessManager(self)
        manager.finished[QNetworkReply].connect(self.cacheImgs)

        for url in urls:
            manager.get(QNetworkRequest(url))

    @Slot(QNetworkReply)
    def cacheImgs(self, reply):
        self.sheetImgs.append((reply.url(), reply.readAll()))

        print("Downloaded {} of {} images".format(len(self.sheetImgs), self.numSheetImgs))
        if (self.numSheetImgs == len(self.sheetImgs)):
            print("Finished downloading images")
            self.sheetDownloadFinished.connect(self.writePdf)
            self.sheetDownloadFinished.emit()

    @Slot()
    def writePdf(self):
        printer = QPrinter()
        printer.setOutputFormat(QPrinter.PdfFormat)
        printer.setOutputFileName(QStandardPaths.writableLocation(QStandardPaths.MusicLocation) + "/" + self.fileName)
        printer.setPageSize(QPagedPaintDevice.A4)

        print("Saving to", printer.outputFileName())

        painter = QPainter()
        painter.begin(printer)

        currentPage: int = 1
        self.sheetImgs.sort()

        for img in self.sheetImgs:
            image = QImage.fromData(img[1], format="PNG")

            # TODO: Scale the image to actually fit the page in all cases
            painter.drawImage(0, 0, image)

            if (currentPage < self.numSheetImgs):
                printer.newPage()

            print("printed page {}".format(currentPage))

            currentPage += 1

        painter.end()

    @Slot(QNetworkReply)
    def replyFinished(self, reply):
        path = QStandardPaths.writableLocation(QStandardPaths.MusicLocation) + "/" + self.fileName

        file = QSaveFile()
        file.setFileName(path)
        file.open(QIODevice.WriteOnly)
        file.write(reply.readAll())
        file.commit()

        self.writeFinished.emit()
