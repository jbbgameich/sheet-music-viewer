import org.kde.kirigami 2.8 as Kirigami
import QtQuick.Controls 2.2 as Controls
import QtQuick.Layouts 1.2
import QtQuick 2.7

import org.kde.scores 1.0

Kirigami.ScrollablePage {
    title: "Search Sheet Music"
    header: Kirigami.SearchField {
        id: searchField
        onAccepted: {
            // We can't send this imiidiately to the model, it would start to many requests
            searchModel.searchText = text
        }
    }

    Component {
        id: sheetComponent
        Sheet {}
    }

    ListView {
        model: SearchModel {
            id: searchModel
        }
        ListView.onAdd: {
            console.log(rowCount())
        }
        delegate: Kirigami.BasicListItem {
            height: Kirigami.Units.gridUnit * 4
            onClicked: pageStack.push(sheetComponent, {
                // HACK
                "numPages": parseInt(
                    model.pages.includes("pages")
                        ? model.pages.replace(" pages", "")
                        : model.pages.replace(" page", "")
                    ),
                "url": model.url,
                "title": model.title
            })

            contentItem: RowLayout {
                Layout.fillWidth: true

                Kirigami.Icon {
                    Layout.fillHeight: true
                    height: Kirigami.Units.iconSizes.large
                    width: height
                    source: model.image
                }
                ColumnLayout {
                    Kirigami.Heading {
                        level: 3
                        maximumLineCount: 1
                        Layout.fillWidth: true
                        elide: Text.ElideRight
                        text: model.title
                    }
                    Controls.Label {
                        wrapMode: Text.WordWrap
                        elide: Text.ElideRight
                        text: model.description ? model.description : qsTr("No description available")
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                    }
                }
            }
        }
    }
}
