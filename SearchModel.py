from PySide2.QtCore import Property, QAbstractListModel, Qt, QModelIndex, QByteArray
from scraper import Search, Result

class SearchModel(QAbstractListModel):
    __search = Search()

    def readSearchText(self) -> str:
        return self.__search.query()

    def setSearchText(self, text: str):
        self.__search.set_query(text)

        self.beginResetModel()
        self.__search.fetch()
        self.endResetModel()

        #print(len(self.__search.results()))

    searchText = Property(str, readSearchText, setSearchText)


    def roleNames(self):
        roleNames = {}
        roleNames[Qt.UserRole + 1] = QByteArray("title".encode("utf-8"))
        roleNames[Qt.UserRole + 2] = QByteArray("description".encode("utf-8"))
        roleNames[Qt.UserRole + 3] = QByteArray("url".encode("utf-8"))
        roleNames[Qt.UserRole + 4] = QByteArray("parts".encode("utf-8"))
        roleNames[Qt.UserRole + 5] = QByteArray("pages".encode("utf-8"))
        roleNames[Qt.UserRole + 6] = QByteArray("time".encode("utf-8"))
        roleNames[Qt.UserRole + 7] = QByteArray("age".encode("utf-8"))
        roleNames[Qt.UserRole + 8] = QByteArray("views".encode("utf-8"))
        roleNames[Qt.UserRole + 9] = QByteArray("instruments".encode("utf-8"))
        roleNames[Qt.UserRole + 10] = QByteArray("image".encode("utf-8"))

        return roleNames

    def data(self, index, role):
        if (index.isValid()):
            if (role == Qt.UserRole + 1):
                return self.__search.results()[index.row()].title
            elif (role == Qt.UserRole + 2):
                return self.__search.results()[index.row()].description
            elif (role == Qt.UserRole + 3):
                return self.__search.results()[index.row()].url
            elif (role == Qt.UserRole + 4):
                return self.__search.results()[index.row()].parts
            elif (role == Qt.UserRole + 5):
                return self.__search.results()[index.row()].pages
            elif (role == Qt.UserRole + 6):
                return self.__search.results()[index.row()].time
            elif (role == Qt.UserRole + 7):
                return self.__search.results()[index.row()].age
            elif (role == Qt.UserRole + 8):
                return self.__search.results()[index.row()].views
            elif (role == Qt.UserRole + 9):
                return self.__search.results()[index.row()].instruments
            elif (role == Qt.UserRole + 10):
                return self.__search.results()[index.row()].image
            else:
                return str()

    def rowCount(self, index):
        return len(self.__search.results())
