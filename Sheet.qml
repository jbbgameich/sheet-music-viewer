import org.kde.kirigami 2.8 as Kirigami
import QtQuick.Controls 2.2 as Controls
import QtQuick.Layouts 1.2
import QtQuick 2.7

import QtMultimedia 5.7

import org.kde.scores 1.0

Kirigami.ScrollablePage {
    id: sheetPage
    property int numPages
    property url url

    Downloader {
        id: downloader
    }

    Connections {
        target: downloader
        onWriteFinished: showPassiveNotification(qsTr("Download finished"))
    }

    contextualActions: [
        Kirigami.Action {
            text: qsTr("Download Audio")
            onTriggered: downloader.download(sheetModel.audioUrl, title + ".mp3")
        },
        Kirigami.Action {
            text: qsTr("Download Sheet Music")
            onTriggered: {
                downloader.downloadSheet(sheetModel.pageUrls, title)
            }
        }
    ]

    Component.onCompleted: console.log(sheetModel.rowCount())
    ListView {
        model: SheetModel {
            id: sheetModel
            numPages: sheetPage.numPages
            url: sheetPage.url
        }
        delegate: Image {
            fillMode: Image.PreserveAspectFit

            source: model.image
            width: parent.width
        }
    }

    footer: Item {
        height: Kirigami.Units.gridUnit * 2
        RowLayout {
            id: playerLayout
            anchors.fill: parent

            Audio {
                id: player
                source: sheetModel.audioUrl
            }

            Controls.ToolButton {
                Layout.alignment: Qt.AlignLeft
                icon.name: player.playbackState == Audio.PlayingState
                    ? "media-playback-pause"
                    : "media-playback-start"
                onClicked: player.playbackState == Audio.PlayingState
                    ? player.pause()
                    : player.play()
            }
            Controls.ToolButton {
                Layout.alignment: Qt.AlignLeft
                icon.name: "media-skip-backward"
            }
            Controls.Slider {
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignRight
                from: 0
                to: player.duration
                value: player.position
                onValueChanged: {
                    player.seek(value)
                }
            }
        }
        Rectangle {
            z: -1

            Kirigami.Theme.colorSet: Kirigami.Theme.Complementary
            color: Kirigami.Theme.complementaryBackgoundColor
            anchors.fill: parent
        }
    }
}
