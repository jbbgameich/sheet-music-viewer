from PySide2.QtCore import Property, QAbstractListModel, Qt, QModelIndex, QByteArray
from scraper import SheetFetcher, Sheet, Page

class SheetModel(QAbstractListModel):
    __fetcher = SheetFetcher()

    def readNumPages(self) -> str:
        return str(self.__fetcher.num_pages())

    def setNumPages(self, num: str):
        self.__fetcher.set_num_pages(int(num))

    # Property type int segfaults, don't ask why
    numPages = Property(str, readNumPages, setNumPages)

    def readUrl(self) -> str:
        return self.__fetcher.url()

    def writeUrl(self, url: str):
        self.__fetcher.set_url(url)

        self.beginResetModel()
        self.__fetcher.fetch()
        self.endResetModel()

    url = Property(str, readUrl, writeUrl)

    def readAudioUrl(self) -> str:
        return self.__fetcher.sheet().audio_url

    audioUrl = Property(str, readAudioUrl, constant=True)

    def readPageUrls(self) -> list:
        return [page.image for page in self.__fetcher.sheet().pages]

    pageUrls = Property("QVariantList", readPageUrls, constant=True)

    def roleNames(self):
        roleNames = {}
        roleNames[Qt.UserRole + 1] = QByteArray("image".encode("utf-8"))

        return roleNames

    def data(self, index, role):
        if (index.isValid()):
            if (role == Qt.UserRole + 1):
                return self.__fetcher.sheet().pages[index.row()].image
            else:
                return str()

    def rowCount(self, index):
        return len(self.__fetcher.sheet().pages)
