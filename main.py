#!/usr/bin/env python3

import signal
from PySide2.QtGui import QGuiApplication
from PySide2.QtQml import QQmlApplicationEngine, qmlRegisterType
import sys


##### HACK ####
import subprocess
subprocess.call(["pyside2-rcc", "resources.qrc", "-o", "resources.py"])
##### END HACK #####

import resources
from SearchModel import SearchModel
from SheetModel import SheetModel
from Downloader import Downloader

if __name__ == "__main__":
    app = QGuiApplication(sys.argv)

    # Quit on Ctrl + c
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    qmlRegisterType(SearchModel, "org.kde.scores", 1, 0, "SearchModel")
    qmlRegisterType(SheetModel, "org.kde.scores", 1, 0, "SheetModel")
    qmlRegisterType(Downloader, "org.kde.scores", 1, 0, "Downloader")

    engine = QQmlApplicationEngine()
    engine.load("main.qml")

    if len(engine.rootObjects()) == 0:
        quit()

    app.exec_()

else:
    print("This is not a python module, start it as standalone application instead")
