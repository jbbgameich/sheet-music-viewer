import org.kde.kirigami 2.2 as Kirigami
import QtQuick.Controls 2.2 as Controls
import QtQuick 2.7

Kirigami.ApplicationWindow {
	title: qsTr("Musescore")

	pageStack.initialPage: Search {}
}
