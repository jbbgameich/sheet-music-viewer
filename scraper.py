import requests
import urllib.parse
from bs4 import BeautifulSoup
import json

class struct:
    def __str__(self):
        return '\n'.join("%s: %s" % item for item in vars(self).items())

class Result(struct):
    title: str
    description: str
    url: str
    image: str

    parts: str
    pages: str
    time: str
    age: str
    views: str
    instruments: str

    def __str__(self):
        return '\n'.join("%s: %s" % item for item in vars(self).items())


class Search:
    __query: str = ""
    __base_url: str = "https://musescore.com"
    __soup: BeautifulSoup
    __results: list = []

    def __init__(self, query: str = ""):
        if (query):
            self.set_query(query)

    """
    Sets the search string
    """
    def set_query(self, query: str):
        self.__query = query

    """
    Returns the current search string
    """
    def query(self):
        return self.__query

    """
    Sends the request and stores the data, which is then retrievable from
    results()
    """
    def fetch(self):
        if (len(self.__query) > 0):
            self.__results.clear()

            url = self.__base_url + "/sheetmusic?text={}".format(urllib.parse.quote_plus(self.__query))

            self.__soup = BeautifulSoup(requests.get(url).text, features="lxml", parser="html.parser")
            articles = self.__soup.find_all("article")
            
            for article_s in articles:
                result = Result()

                metadata_s: BeautifulSoup = article_s.find("div", class_="score-info__meta")

                title_s: BeautifulSoup = article_s.find("h2", class_="score-info__title").find("a")
                result.title = title_s.string
                result.url = self.__base_url + title_s["href"]
                if (result.title):
                    result.title = result.title.strip()

                result.image = article_s.find("source", type="image/jpeg")["srcset"].split(" ")[0]

                result.description = article_s.find("div", class_="score-text__content").string
                if (result.description):
                    result.description = result.description.strip()

                for field in metadata_s.find_all("span"):
                    if ("parts" in field.string):
                        result.parts = field.string
                    elif ("page" in field.string):
                        result.pages = field.string
                    elif (":" in field.string):
                        result.time = field.string
                    elif ("ago" in field.string):
                        result.age = field.string
                    elif ("views" in field.string):
                        result.views = field.string

                result.instruments = article_s.find("div", class_="score-info__instruments").string

                #print(result)
                self.__results.append(result)

    """
    Holds the fetched result data
    """
    def results(self):
        return self.__results

class Page(struct):
    image: str

class Author(struct):
    avatar: str
    display_name: str
    url: str

class Sheet(struct):
    author: Author
    audio_url: str
    pages: list
    num_pages: int


class SheetFetcher:
    __url: str
    __num_pages: int = 0
    __soup: BeautifulSoup

    __sheet: Sheet


    def set_url(self, url: str):
        if (len(url) > 0):
            self.__url = url

    def url(self):
        return self.__url

    """
    On the sheet there is no way to find out how many pages we need to fetch
    but we need the number to guess the urls.

    If no number of pages is supplied, the scraper will simply try how many pages
    exist, but this is much slower.
    """
    def set_num_pages(self, num: int):
        self.__num_pages = num

    def num_pages(self):
        return self.__num_pages

    """
    Holds the fetched result data
    """
    def sheet(self):
        return self.__sheet

    """
    Sends the request and stores the extracted data,
    which can then be retrieved from sheet()
    """
    def fetch(self):
        if (len(self.__url) > 0):
            # Reset old data
            self.__sheet = Sheet()
            self.__sheet.pages = []

            self.__soup = BeautifulSoup(requests.get(self.__url).text, features="lxml", parser="html.parser")

            page_s = self.__soup.find("div", class_="viewerInner").find("div", class_="page")
            image_base_url = page_s.find("div", class_="image").find("img")["src"]

            if (self.__num_pages > 0):
                for i in range(self.__num_pages):
                    page = Page()
                    page.image = image_base_url.replace("score_0", "score_" + str(i))
                    self.__sheet.pages.append(page)

                self.__sheet.num_pages = self.num_pages
            else:
                i = 0
                while requests.head(image_base_url.replace("score_0", "score_" + str(i))).status_code == 200:
                    page = Page()
                    page.image = image_base_url.replace("score_0", "score_" + str(i))
                    print(i, page.image)
                    self.__sheet.pages.append(page)
                    i += 1

                self.__sheet.num_pages = i - 1

            author_s = self.__soup.find("a", class_="avatar")

            author = Author()
            author.display_name = author_s["title"]
            author.url = author_s["href"]
            author.avatar = author_s["style"].split("background-image: url(")[1][:-1]
            self.__sheet.author = author

            if (".png" in image_base_url):
                self.__sheet.audio_url = image_base_url.replace("score_0.png", "score.mp3")
            else:
                self.__sheet.audio_url = image_base_url.replace("score_0.svg", "score.mp3")

            # Make sure num_pages is correct
            # It contains the number of pages we fetched, which has to match
            # then lenght of the result list. Otherwise something is wrong.
            assert self.__sheet.num_pages, len(self.__sheet.pages)
        else:
            print("You can only call fetch after setting url!")

if __name__ == "__main__":
    musescore_search = Search()
    musescore_search.set_query("Star Sky")
    musescore_search.fetch()

    for i in musescore_search.results():
        print(i.title)

    musescore_sheet = SheetFetcher()
    musescore_sheet.set_url(musescore_search.results()[0].url)
    musescore_sheet.set_num_pages(int(musescore_search.results()[0].pages.replace(" pages", "")))
    musescore_sheet.fetch()


    for page in musescore_sheet.sheet().pages:
        print(page)
